#include <math.h>
#include <uWS/uWS.h>
#include <chrono>
#include <iostream>
#include <fstream>
#include <thread>
#include <vector>
#include "Eigen-3.3/Eigen/Core"
#include "Eigen-3.3/Eigen/QR"
#include "MPC.h"
#include "MPC_Control.h"
#include "json.hpp"

// for convenience
using json = nlohmann::json;
using namespace::std;

namespace {
  const size_t N = 12;
  const double dt = 0.1;
  // This is the length from front to CoG that has a similar radius.
  const double Lf = 2.67;
  const double vel_ref = 100.0;
  const double max_deg = 25.0;

  const unsigned int poly_order = 3;

  const unsigned int num_ref_points = 30;

  const unsigned int actuator_delay = 100;

  // Default weights for state, actuators & actuator smoothness
  const double cte_w = 200.0, epsi_w = 200.0, vel_w = 0.1,
    delta_w = 1.0, acc_w = 1.0,
    delta_dt_w = 15.0, acc_dt_w = 1.0;

  const vector<double> default_weights =
      {cte_w, epsi_w, vel_w, delta_w, acc_w, delta_dt_w, acc_dt_w};
}

// For converting back and forth between radians and degrees.
constexpr double pi() { return M_PI; }
double deg2rad(double x) { return x * pi() / 180; }
double rad2deg(double x) { return x * 180 / pi(); }

// Checks if the SocketIO event has JSON data.
// If there is data the JSON object in string format will be returned,
// else the empty string "" will be returned.
string hasData(string s) {
  auto found_null = s.find("null");
  auto b1 = s.find_first_of("[");
  auto b2 = s.rfind("}]");
  if (found_null != string::npos) {
    return "";
  } else if (b1 != string::npos && b2 != string::npos) {
    return s.substr(b1, b2 - b1 + 2);
  }
  return "";
}

// Evaluate a polynomial.
double polyeval(Eigen::VectorXd coeffs, double x) {
  double result = 0.0;
  for (int i = 0; i < coeffs.size(); i++) {
    result += coeffs[i] * pow(x, i);
  }
  return result;
}

// Fit a polynomial.
// Adapted from
// https://github.com/JuliaMath/Polynomials.jl/blob/master/src/Polynomials.jl#L676-L716
Eigen::VectorXd polyfit(Eigen::VectorXd xvals, Eigen::VectorXd yvals,
                        int order) {
  assert(xvals.size() == yvals.size());
  assert(order >= 1 && order <= xvals.size() - 1);
  Eigen::MatrixXd A(xvals.size(), order + 1);

  for (int i = 0; i < xvals.size(); i++) {
    A(i, 0) = 1.0;
  }

  for (int j = 0; j < xvals.size(); j++) {
    for (int i = 0; i < order; i++) {
      A(j, i + 1) = A(j, i) * xvals(j);
    }
  }

  auto Q = A.householderQr();
  auto result = Q.solve(yvals);
  return result;
}

vector<double> load_weights(const string filename) {
  ifstream is(filename);
  istream_iterator<double> start(is), end;

  return vector<double>(start, end);
}

int main() {
  uWS::Hub h;

  // MPC init; if weights file exists, then load from there; otherwise, use defaults
  const MPC_Control mpcc(N, dt, Lf, vel_ref, max_deg);

  cout << "Printing MPC Control variables: " << mpcc << endl;

  vector<double> weights = load_weights("../weights.txt");
  if (weights.empty()) {
    cout << "Using the default weights!" << endl;
    weights = default_weights;
  }
  cout << "Printing MPC weights: " << endl;
  for (auto const& elem : weights) {
    cout << elem << ' ';
  }
  cout << endl;

  MPC mpc(weights, mpcc);

  h.onMessage([&mpc](uWS::WebSocket<uWS::SERVER> ws, char *data, size_t length,
                     uWS::OpCode opCode) {
    // "42" at the start of the message means there's a websocket message event.
    // The 4 signifies a websocket message
    // The 2 signifies a websocket event
    string sdata = string(data).substr(0, length);
    cout << sdata << endl;
    if (sdata.size() > 2 && sdata[0] == '4' && sdata[1] == '2') {
      string s = hasData(sdata);
      if (s != "") {
        auto j = json::parse(s);
        string event = j[0].get<string>();
        if (event == "telemetry") {
          // j[1] is the data JSON object
          vector<double> ptsx = j[1]["ptsx"];
          vector<double> ptsy = j[1]["ptsy"];

          const double px = j[1]["x"], py = j[1]["y"],
            psi = j[1]["psi"], vel = j[1]["speed"],
            delta = j[1]["steering_angle"], acc = j[1]["throttle"];

          // Co-ordinate transformation
          auto sz = ptsx.size();
          Eigen::VectorXd ptsx_t(sz), ptsy_t(sz);
          for (unsigned int i = 0; i < sz; i++) {
            const double dx = ptsx[i] - px, dy = ptsy[i] - py;

            ptsx_t[i] = dx * cos(psi) + dy * sin(psi);
            ptsy_t[i] = dy * cos(psi) - dx * sin(psi);
          }

          // Calculate coefficients and ref measurements
          const auto coeffs = polyfit(ptsx_t, ptsy_t, poly_order);
          const double cte = coeffs[0], epsi = -atan(coeffs[1]);

          // Predicted state after simulated latency
          const double px_delay = 0.0 + vel * dt, py_delay = 0.0,
            psi_delay = 0.0 + vel * -delta / Lf * dt,
            vel_delay = vel + acc * dt,
            cte_delay = cte + vel * sin(epsi) * dt,
            epsi_delay = epsi - vel * delta * dt  / Lf;

          Eigen::VectorXd state(6);
          state << px_delay, py_delay, psi_delay, vel_delay, cte_delay, epsi_delay;

          auto solution = mpc.Solve(state, coeffs);

          const double steer_value = solution[0] / (deg2rad(max_deg) * Lf);
          const double throttle_value = solution[1];

          // Display the MPC predicted trajectory
          vector<double> mpc_x_vals = {state[0]};
          vector<double> mpc_y_vals = {state[1]};
          // Our MPC points for simulator (green)
          for (unsigned int i = 2; i < solution.size(); i+=2) {
            mpc_x_vals.push_back(solution[i]);
            mpc_y_vals.push_back(solution[i+1]);
          }

          // Display reference line (yellow)
          vector<double> next_x_vals;
          vector<double> next_y_vals;
          for (unsigned int i = 1; i < num_ref_points; i++) {
            next_x_vals.push_back(poly_order * i);
            next_y_vals.push_back(polyeval(coeffs, poly_order * i));
          }

          json msgJson;
          msgJson["steering_angle"] = steer_value;
          msgJson["throttle"] = throttle_value;
          msgJson["mpc_x"] = mpc_x_vals;
          msgJson["mpc_y"] = mpc_y_vals;
          msgJson["next_x"] = next_x_vals;
          msgJson["next_y"] = next_y_vals;


          auto msg = "42[\"steer\"," + msgJson.dump() + "]";
          cout << msg << endl;
          // Latency
          // The purpose is to mimic real driving conditions where
          // the car does actuate the commands instantly.
          //
          // Feel free to play around with this value but should be to drive
          // around the track with 100ms latency.
          //
          // NOTE: REMEMBER TO SET THIS TO 100 MILLISECONDS BEFORE
          // SUBMITTING.
          this_thread::sleep_for(chrono::milliseconds(actuator_delay));
          ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
        }
      } else {
        // Manual driving
        string msg = "42[\"manual\",{}]";
        ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
      }
    }
  });

  // We don't need this since we're not using HTTP but if it's removed the
  // program
  // doesn't compile :-(
  h.onHttpRequest([](uWS::HttpResponse *res, uWS::HttpRequest req, char *data,
                     size_t, size_t) {
    const string s = "<h1>Hello world!</h1>";
    if (req.getUrl().valueLength == 1) {
      res->end(s.data(), s.length());
    } else {
      // i guess this should be done more gracefully?
      res->end(nullptr, 0);
    }
  });

  h.onConnection([&h](uWS::WebSocket<uWS::SERVER> ws, uWS::HttpRequest req) {
    cout << "Connected!!!" << endl;
  });

  h.onDisconnection([&h](uWS::WebSocket<uWS::SERVER> ws, int code,
                         char *message, size_t length) {
    ws.close();
    cout << "Disconnected" << endl;
  });

  int port = 4567;
  if (h.listen(port)) {
    cout << "Listening to port " << port << endl;
  } else {
    cerr << "Failed to listen to port" << endl;
    return -1;
  }
  h.run();
}
