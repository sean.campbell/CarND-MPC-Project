#!/bin/bash
cd `dirname $0`

if [ ! -d 'build' ]; then
  ./make.sh
fi

cd build
./mpc
