#include "MPC_Control.h"

#include <cmath>

MPC_Control::MPC_Control(const size_t N, const double dt, const double Lf,
            const double vel_ref, const double max_deg) :
            N(N), dt(dt), Lf(Lf), vel_ref(vel_ref),
            n_vars(N * 6 + (N - 1) * 2), n_constraints(N * 6),
            delta_max(max_deg * M_PI / 180), delta_min(-(max_deg * M_PI / 180)),
            x_start(0), y_start(N),
            psi_start(2*N), vel_start(3*N),
            cte_start(4*N), epsi_start(5*N),
            delta_start(6*N), acc_start(7 * N - 1) {};

MPC_Control::~MPC_Control() {}

std::ostream& operator <<(std::ostream &strm, const MPC_Control &mpcc) {
  return strm << "MPC_Control("
              << "N: " << mpcc.N << ", "
              << "dt: " << mpcc.dt << ", "
              << "Lf: " << mpcc.Lf << ", "
              << "Vel_ref: " << mpcc.vel_ref << ", "
              << "n_vars: " << mpcc.n_vars << ", "
              << "n_constraints: " << mpcc.n_constraints << ", "
              << "delta_max: " << mpcc.delta_max << ", "
              << "delta_min: " << mpcc.delta_min << ")";
}
