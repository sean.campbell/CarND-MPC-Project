#ifndef MPC_CONTROL_H
#define MPC_CONTROL_H

#include <ostream>
#include <cstddef>

class MPC_Control {
public:
  MPC_Control(const size_t N, const double dt, const double Lf,
              const double vel_ref, const double max_deg);

  virtual ~MPC_Control();

  friend std::ostream& operator <<(std::ostream& outputStream, const MPC_Control& mpcc);

  const size_t N;
  const double dt, Lf, vel_ref;

  // Calculated
  const size_t n_vars, n_constraints,
    x_start, y_start, psi_start, vel_start,
    cte_start, epsi_start, delta_start, acc_start;
  const double delta_max, delta_min;
};

#endif
