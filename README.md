# CarND-Controls-MPC

Self-Driving Car Engineer Nanodegree Program

---

![Sample](/output/sample.gif)

# Overview

[Model Predictive Control](https://en.wikipedia.org/wiki/Model_predictive_control) (MPC) is a process control algorithm based on iterative, finite-horizon optimization of a plant model.  Essentially, at each time-step, the controller solves an open-loop optimization problem for the prediction horizon and applies the first of the computed actuator outputs.  At the subsequent time-step, the system state is sampled and the above is re-computed, throwing away the prior predictions since the complexities of reality may not have been adequately captured by the dynamic model.  The prediction horizon is therefore continuously shifted forward, hence leading to its other name of Receding Horizon Control.

MPC has a number of advantages such as explicitly handling constraints and having a small number of tuning parameters, namely the prediction horizon, time-step duration and the cost function itself.  For comparison, PID controllers are much simpler and computationally faster but less accurate as they do not consider future time-steps and physical constraints.

An MPC was employed in this project in order to successfully drive a simulated vehicle around a track without incident in a reasonable period of time.  The simulator outputs telemetry information, namely positional, orientation, velocity and current actuator outputs in the form of steering and acceleration.  The simulator expects the subsequent actuator outputs as inputs and additionally supports vectors for overlaying the planned route per time-step.

This project involved the Term 2 Simulator which can be downloaded [here](https://github.com/udacity/self-driving-car-sim/releases).

[uWebSocketIO](https://github.com/uWebSockets/uWebSockets) must additionally be compiled for your architecture.  Scripts are available [here](https://github.com/udacity/CarND-MPC-Project) for Windows and Debian.  For CentOS, libssl-dev --> openssl-devel.  Further, IPOPT and CppAD must be installed, with the relevant instructions available [here](./install_Ipopt_CppAD.md).

# Compiling and executing the project

Simply run ./build.sh && ./run.sh while the simulator is running.

# Implementation

## Model

A basic kinematic model has been used, with complex interactions in the environment ignored such as slip angle and tyre forces.  The state of the model consists of:
* `x, y`  : Position
* `psi`   : Orientation
* `v`     : Velocity
* `cte`   : Cross-Track Error (CTE)
* `epsi`  : Orientation Error

The update equations are:
```
x[t] = x[t-1] + v[t-1] * cos(psi[t-1]) * dt
y[t] = y[t-1] + v[t-1] * sin(psi[t-1]) * dt
psi[t] = psi[t-1] + v[t-1] / Lf * delta[t-1] * dt
v[t] = v[t-1] + a[t-1] * dt

cte[t] = f(x[t-1]) - y[t-1] + v[t-1] * sin(epsi[t-1]) * dt
epsi[t] = psi[t] - psid[t-1] + v[t-1] * delta[t-1] / Lf * dt
```

where the additional constants are:
* `Lf`      : The distance between the center of mass and the front axle
* `dt`      : The time-step

and the additional terms are:
* `f(x[t-1])` : A fitted polynomial evaluated at `x[t-1]`
* `psid[t - 1]`: The arc-tangent of the derivative of the polynomial evaluated at `x[t - 1]`

The outputs of the model are:
* `a`       : Acceleration
* `delta`   : Steering angle

The optimization problem is therefore finding values for these outputs such that a cost function comprised of the model state is and outputs is minimized, with weighted components:
* Square sum of `cte` and `epsi` in order to heavily penalize deviation from the ground truth.  Heavy weights were given to these terms as they are arguably the most crucial to keeping the vehicle on track.
* Square sum of a difference between `v` and a reference velocity, statically set at an ambitious 100 mph.  This weight was set relatively low, ensuring that in the event of satisfying the other constraints, the model would subsequently favor those with slightly higher velocities.  Further, it ensures that the vehicle will not simply grind to a halt if the other cost terms were minimized.
* Square sum of the actuator outputs in order to penalize high magnitudes in the outputs.  Relatively low weightings were given here but the model does not seem prone to extreme actions.
* Square sum of the first order difference between consecutive actuator outputs in order to penalize drastic changes in output, ensuring a slight smoothness.  A heavier weighting to steering was tuned in the output.

The cost weights for the terms were manually tuned, with the constants being extracted out and loaded from a file in order to skip compilation when changing constants.  If the file is not present, default weights are assumed.  Note that the cost function is located [here](./src/MPC.cpp#L82), weightings are supplied through the a vector and control parameters in addition to model constraints are passed through the basic [container class](./src/MPC_Control.h) for [MPC](./src/MPC.h).

The true work is performed by the Interior Point Optimizer (Ipopt) which minimizes the constructed cost function while simultaneously considering the supplied constraints on the model state and actuator outputs in order to make accurate predictions.  

## Polynomial Fitting

The global waypoints provided by the simulator are transformed into the vehicle co-ordinate system.  This is simply accomplished through the 2D vector transformation equations:
* ptsx_t[i] = dx * cos(psi) + dy * sin(psi);
* ptsy_t[i] = dy * cos(psi) - dx * sin(psi);

Note that the signs of the transformations are flipped due to an initial `-psi` from the simulator; therefore cos(-x)==cos(x) & sin(-x)==-sin(x).

A cubic polynomial is subsequently fitted to the transformed points, since cubic functions will allow curves and higher order polynomials are expensive, can cause erratic line fittings and often run into numerical issues.

The `cte` is effectively the zero-order coefficient of the cubic and the `epsi` is estimated through taking the negative arc-tangent of the first-order coefficient.

## Latency

In order to account for latency, the state values are simply calculated using the model equations and the simulated latency magnitude.  The equations can be simplified due to several values being equivalent to zero.  The subsequent output of those equations is then supplied to the MPC model.

## Horizon

The horizon is composed of the time-steps `N` and the time-step duration `dt`, with the horizon length being the product of the two.  Through manually driving around the track and considering the sharper turns, it seemed like a horizon length of at least a few seconds would be necessary, especially at large speeds.  This led to an initial estimate of 20 for N and 0.2 for dt.  Through experimentation, this high number of time-steps was not necessary and led to redundant computations, lowering the number to 12.  The time-step magnitude additionally caused the model to be quite slow to react, with the final tuning leading to a magnitude of 0.1.  Together, this leads to a horizon with duration 1.2 seconds, which was surprising given the velocity the vehicle was capable of achieving and the sharp turns on the track.

# Results

A single lap sample is available [here](/output/output.mp4).  I strictly aimed to not touch the red markings, ensure the model did not deviate excessively from the reference and still maintained a sufficient velocity.  Really enjoyed this particular project! :-)
