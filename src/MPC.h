#ifndef MPC_H
#define MPC_H

#include <vector>
#include "MPC_Control.h"
#include "Eigen-3.3/Eigen/Core"

class MPC {
 public:
  MPC(const std::vector<double> &weights, const MPC_Control &control);

  virtual ~MPC();

  // Solve the model given an initial state and polynomial coefficients.
  // Return the first actuatotions.
  std::vector<double> Solve(Eigen::VectorXd state, Eigen::VectorXd coeffs);

private:
  struct MPC_Internals;
  const std::vector<double> weights;
  const MPC_Control control;
};

#endif /* MPC_H */
