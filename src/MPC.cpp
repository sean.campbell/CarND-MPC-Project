#include "MPC.h"
#include <cppad/cppad.hpp>
#include <cppad/ipopt/solve.hpp>
#include <limits>
#include "Eigen-3.3/Eigen/Core"

using CppAD::AD;
using namespace std;

namespace {
  const string ipopt_options = "Integer print_level  0\n"
  "Sparse  true        forward\nSparse  true        reverse\n"
  "Numeric max_cpu_time          0.5\n";

  typedef CPPAD_TESTVECTOR(double) Dvector;
}

struct MPC::MPC_Internals {
  static Eigen::VectorXd poly_derivative(const Eigen::VectorXd &coeffs) {
    Eigen::VectorXd deriv(coeffs.size()-1);
    for(int i = 0; i < coeffs.size()-1; i++) {
        deriv(i) = coeffs(i+1) * (i+1);
    }
    return deriv;
  }
};

class FG_eval {
 public:
  // Fitted polynomial coefficients
  const Eigen::VectorXd coeffs, derivCoeffs;
  const vector<double> weights;
  const MPC_Control control;

  FG_eval(const Eigen::VectorXd &coeffs, const Eigen::VectorXd &derivCoeffs,
  const vector<double> weights, const MPC_Control &control)
    : coeffs(coeffs), derivCoeffs(derivCoeffs),
      weights(weights), control(control) {}

  typedef CPPAD_TESTVECTOR(AD<double>) ADvector;
  void operator()(ADvector& fg, const ADvector& vars) {
    fg[0] = cost_function(vars);

    // Model constraints
    fg[1 + this->control.x_start] = vars[this->control.x_start];
    fg[1 + this->control.y_start] = vars[this->control.y_start];
    fg[1 + this->control.psi_start] = vars[this->control.psi_start];
    fg[1 + this->control.vel_start] = vars[this->control.vel_start];
    fg[1 + this->control.cte_start] = vars[this->control.cte_start];
    fg[1 + this->control.epsi_start] = vars[this->control.epsi_start];

    // The rest of the constraints
    for (unsigned int t = 1; t < this->control.N; t++) {
      // State at time t+1
      const AD<double> x1 = vars[this->control.x_start + t], y1 = vars[this->control.y_start + t],
        psi1 = vars[this->control.psi_start + t], v1 = vars[this->control.vel_start + t],
        cte1 = vars[this->control.cte_start + t], epsi1 = vars[this->control.epsi_start + t];

      // State at time t
      const AD<double> x0 = vars[this->control.x_start + t - 1], y0 = vars[this->control.y_start + t - 1],
        psi0 = vars[this->control.psi_start + t - 1], v0 = vars[this->control.vel_start + t - 1],
        cte0 = vars[this->control.cte_start + t - 1], epsi0 = vars[this->control.epsi_start + t - 1];

      // Actuator constraints at time t only
      const AD<double> delta0 = vars[this->control.delta_start + t - 1], a0 = vars[this->control.acc_start + t - 1];

      // N-degree polynomial and derivative
      const AD<double> f0 = eval_poly(this->coeffs, x0);
      const AD<double> psi_des0 = CppAD::atan(eval_poly(this->derivCoeffs, x0));

      // Setting up the rest of the model constraints
      fg[1 + this->control.x_start + t] = x1 - (x0 + v0 * CppAD::cos(psi0) * this->control.dt);
      fg[1 + this->control.y_start + t] = y1 - (y0 + v0 * CppAD::sin(psi0) * this->control.dt);
      fg[1 + this->control.psi_start + t] = psi1 - (psi0 - v0 * delta0 / this->control.Lf * this->control.dt);
      fg[1 + this->control.vel_start + t] = v1 - (v0 + a0 * this->control.dt);
      fg[1 + this->control.cte_start + t] = cte1 - ((f0-y0) + (v0 * CppAD::sin(epsi0) * this->control.dt));
      fg[1 + this->control.epsi_start + t] = epsi1 - ((psi0 - psi_des0) - v0 * delta0 / this->control.Lf * this->control.dt);
    }
  }

private:
  AD<double> cost_function(const ADvector& vars) {
    AD<double> cost = 0.0;
    for (unsigned int t = 0; t < this->control.N; t++) {
      cost += this->weights[0] * CppAD::pow(vars[this->control.cte_start + t], 2);
      cost += this->weights[1] * CppAD::pow(vars[this->control.epsi_start + t], 2);
      cost += this->weights[2] * CppAD::pow(vars[this->control.vel_start + t] - this->control.vel_ref, 2);
      if (t < this->control.N - 1) {
        // Cost based on actuators
        cost += this->weights[3] * CppAD::pow(vars[this->control.delta_start + t], 2);
        cost += this->weights[4] * CppAD::pow(vars[this->control.acc_start + t], 2);
        if (t < this->control.N - 2) {
          // Cost based on smoothness desire
          cost += this->weights[5] * CppAD::pow(
                vars[this->control.delta_start + t + 1] - vars[this->control.delta_start + t], 2);
          cost += this->weights[6] * CppAD::pow(
            vars[this->control.acc_start + t + 1] - vars[this->control.acc_start + t], 2);
        }
      }
    }
    return cost;
  }

  AD<double> eval_poly(const Eigen::VectorXd &coeffs, const AD<double> &x) {
    AD<double> result = 0.0;
    // Using unsigned for the loop index leads to a bizarre seg-fault:
    // yq = f.Forward(q, xq): has a non-zero order Taylor coefficient
    for (int i = 0; i < coeffs.size(); i++) {
        result += coeffs[i] * CppAD::pow(x, i);
    }
    return result;
  }
};

//
// MPC class definition implementation.
//
MPC::MPC(const vector<double> &weights, const MPC_Control &control)
  : weights(weights), control(control) {};
MPC::~MPC() {}

vector<double> MPC::Solve(Eigen::VectorXd state, Eigen::VectorXd coeffs) {
  bool ok = true;

  const double x = state[0], y = state[1],
    psi = state[2], vel = state[3],
    cte = state[4], epsi = state[5];

  // Initial value of the independent variables.
  // SHOULD BE 0 besides initial state.
  Dvector vars(this->control.n_vars);
  for (unsigned int i = 0; i < this->control.n_vars; ++i) {
    vars[i] = 0.0;
  }

  Dvector vars_lowerbound(this->control.n_vars);
  Dvector vars_upperbound(this->control.n_vars);
  // Non-actuator limits
  for (unsigned int i = 0; i < this->control.delta_start; ++i) {
    vars_lowerbound[i] = numeric_limits<int>::min();
    vars_upperbound[i] = numeric_limits<int>::max();
  }
  // Steering limits
  for (unsigned int i = this->control.delta_start; i < this->control.acc_start; ++i) {
    vars_lowerbound[i] = this->control.delta_min;
    vars_upperbound[i] = this->control.delta_max;
  }
  // Accel limits
  for (unsigned int i = this->control.acc_start; i < this->control.n_vars; ++i) {
    vars_lowerbound[i] = -1.0;
    vars_upperbound[i] = 1.0;
  }

  // Lower and upper limits for the constraints
  // Should be 0 besides initial state.
  Dvector constraints_lowerbound(this->control.n_constraints);
  Dvector constraints_upperbound(this->control.n_constraints);

  for (unsigned int i = 0; i < this->control.n_constraints; ++i) {
    constraints_lowerbound[i] = 0;
    constraints_upperbound[i] = 0;
  }

  // Start lower and upper limits at current values
  constraints_lowerbound[this->control.x_start] = x;
  constraints_lowerbound[this->control.y_start] = y;
  constraints_lowerbound[this->control.psi_start] = psi;
  constraints_lowerbound[this->control.vel_start] = vel;
  constraints_lowerbound[this->control.cte_start] = cte;
  constraints_lowerbound[this->control.epsi_start] = epsi;

  constraints_upperbound[this->control.x_start] = x;
  constraints_upperbound[this->control.y_start] = y;
  constraints_upperbound[this->control.psi_start] = psi;
  constraints_upperbound[this->control.vel_start] = vel;
  constraints_upperbound[this->control.cte_start] = cte;
  constraints_upperbound[this->control.epsi_start] = epsi;

  // Calculate derivatives of polynomial
  const Eigen::VectorXd derivCoeffs = MPC_Internals::poly_derivative(coeffs);
  // object that computes objective and constraints
  FG_eval fg_eval(coeffs, derivCoeffs, this->weights, this->control);

  // place to return solution
  CppAD::ipopt::solve_result<Dvector> solution;

  // solve the problem
  CppAD::ipopt::solve<Dvector, FG_eval>(
      ipopt_options, vars, vars_lowerbound, vars_upperbound, constraints_lowerbound,
      constraints_upperbound, fg_eval, solution);

  // Check some of the solution values
  ok &= solution.status == CppAD::ipopt::solve_result<Dvector>::success;

  // Cost
  auto cost = solution.obj_value;
  cout << "Cost " << cost << endl;

  vector<double> ret;
  ret.push_back(solution.x[this->control.delta_start]);
  ret.push_back(solution.x[this->control.acc_start]);
  for (unsigned int i = 0; i < this->control.N; ++i) {
    ret.push_back(solution.x[this->control.x_start + i]);
    ret.push_back(solution.x[this->control.y_start + i]);
  }
  return ret;
}
